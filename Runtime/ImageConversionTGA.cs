using System.IO;
using UnityEngine;

namespace Monoid.Unity.Texture {

  public static class ImageConversionTGA {

    public static byte[] EncodeToTGA (this Texture2D tex, bool rle = false) {
      // http://www.paulbourke.net/dataformats/tga/
      // DATA TYPE 2: Unmapped RGB(A)
      // DATA TYPE 10: Run Length Encoded, RGB(A) images

      // TODO: if all pixels have alpha == 1.0 use pixel size of 24 instead of 32

      int width = tex.width;
      int height = tex.height;

      var pixels = tex.GetPixels32 ();
      int n = pixels.Length;
      bool alpha = false;
      for (int i = 0; i < n; i++) {
        if (pixels[i].a < 255) {
          alpha = true;
          break;
        }
      }

      var header = new byte[18] {
        0, // Number of Characters in Identification Field.
        0, // Color Map Type.
        (byte) (rle ? 10 : 2), // Image Type Code.
        // Color Map Specification.
        0,
        0, // Color Map Origin - Integer ( lo-hi ) index of first color map entry.
        0,
        0, // Color Map Length - Integer ( lo-hi ) count of color map entries.
        0, // Number of bits in color map entry. Possible values are 16/24/32.
        // Image Specification.
        0,
        0, // X Origin of Image - Integer ( lo-hi ) X coordinate of the lower left corner.
        0,
        0, // Y Origin of Image - Integer ( lo-hi ) Y coordinate of the lower left corner of the image.
        (byte) (width & 0xFF),
        (byte) ((width >> 8) & 0xFF), // Width of Image - Integer ( lo-hi ) width of the image in pixels.
        (byte) (height & 0xFF),
        (byte) ((height >> 8) & 0xFF), // Height of Image - Integer ( lo-hi ) height of the image in pixels.
        (byte) (alpha ? 32 : 24), // Image Pixel Size - Possible values are 16/24/32.
        (byte) (alpha ? 8 : 0), // Image Descriptor Byte.
        // Bits 3-0 - number of attribute bits associated with each pixel.
        //    For the Targa 16, this would be 0 or 1. For the Targa 24, it should be 0. For the Targa 32, it should be 8.
        // Bit 4    - reserved.  Must be set to 0.
        // Bit 5    - screen origin bit. 0 = Origin in lower left-hand corner. 1 = Origin in upper left-hand corner. Must be 0 for Truevision images.
        // Bits 7-6 - Data storage interleaving flag. 00 = non-interleaved. 01 = two-way (even/odd) interleaving. 10 = four way interleaving. 11 = reserved.
      };

      var stream = new MemoryStream ();
      stream.Write (header, 0, header.Length);

      var pixel = new Color32 ();
      byte seq = 0;
      bool run = false;

      if (rle) {
        var buffer = new byte[(1 + 4 * 128)]; // header + 4 * max sequence
        for (int i = 0; i < n; i++) {
          if (seq == 0) {
            pixel = pixels[i];
            seq = 1;
            run = i + 1 < n ? Equals (pixels[i + 1], pixel) : false;
            continue;
          }
          if (run == Equals (pixels[i], pixel) && seq < 128) {
            if (run || i + 1 == n || !Equals (pixels[i], pixels[i + 1])) { // isn't the start if a new run
              seq++;
              continue;
            }
          }
          stream.Write (buffer, 0, EncodeSeqTGA (buffer, pixels, alpha, i - seq, seq, run));
          pixel = pixels[i];
          seq = 1;
          run = i + 1 < n ? Equals (pixels[i + 1], pixel) : false;
        }
        stream.Write (buffer, 0, EncodeSeqTGA (buffer, pixels, alpha, n - seq, seq, run));
      } else {
        for (int i = 0; i < n; i++) {
          var color = pixels[i];
          stream.WriteByte (color.b);
          stream.WriteByte (color.g);
          stream.WriteByte (color.r);
          if (alpha) {
            stream.WriteByte (color.a);
          }
        }
      }

      //footer
      for (int i = 0; i < 8; i++) {
        stream.WriteByte (0);
      }
      foreach (var c in "TRUEVISION-XFILE.") {
        stream.WriteByte ((byte) c);
      }
      stream.WriteByte (0);

      stream.Flush ();
      return stream.ToArray ();
    }

    private static bool Equals (Color32 a, Color32 b) {
      return a.r == b.r && a.g == b.g && a.b == b.b && a.a == b.a;
    }

    private static int EncodeSeqTGA (byte[] buffer, Color32[] pixels, bool alpha, int off, byte len, bool run) {
      if (len == 0) {
        return 0;
      }
      if (run) {
        var color = pixels[off];
        buffer[0] = (byte) ((len - 1) | (1 << 7));
        buffer[1] = color.b;
        buffer[2] = color.g;
        buffer[3] = color.r;
        if (alpha) {
          buffer[4] = color.a;
          return 5;
        }
        return 4;
      }
      int j = 0;
      buffer[j++] = (byte) (len - 1);
      for (int i = 0; i < len; i++) {
        var color = pixels[off + i];
        buffer[j++] = color.b;
        buffer[j++] = color.g;
        buffer[j++] = color.r;
        if (alpha) {
          buffer[j++] = color.a;
        }
      }
      return j;
    }

  }

}