using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Experimental.AssetImporters;


namespace Monoid.Unity.Texture {

  [CustomEditor(typeof(GradientTexture))]
  public class GradientTextureEditor : ScriptedImporterEditor {

    #region -Create Asset in Project View-

    [MenuItem ("Assets/Create/Mosaic/Gradient Texture", priority = 9000)]
    public static void Create () {
      var folder = AssetDatabase.GetAssetPath (Selection.activeObject);
      var name = "gradient";

      File.WriteAllText(Path.Combine (folder, name + ".gradtex"), string.Empty);
    }

    [MenuItem ("Assets/Create/Mosaic/Gradient Texture", true, priority = 9000)]
    public static bool CanCreate () {
      var folder = AssetDatabase.GetAssetPath (Selection.activeObject);
      return AssetDatabase.IsValidFolder (folder);
    }

    #endregion

    public override void OnInspectorGUI() {
      serializedObject.Update();
      DrawPropertiesExcluding(serializedObject);
      serializedObject.ApplyModifiedProperties();

      ApplyRevertGUI();

      EditorGUILayout.Separator();

      if (GUILayout.Button("Export")) {
        var t = (GradientTexture)target;
        var path = t.assetPath;
        var dir = Path.GetDirectoryName(path);
        var file = Path.GetFileNameWithoutExtension(path);
        var save = EditorUtility.SaveFilePanel("Export Gradient Texture", dir, file, "png");
        if (save.Length > 0) {
          var format = Gradients.IsOpaque(t.gradient) ? TextureFormat.RGB24 : TextureFormat.RGBA32;
          var texture = new Texture2D(t.size.x, t.size.y, format, false);
          Gradients.Sample(t.gradient, texture);
          File.WriteAllBytes(save, texture.EncodeToPNG());
        }
      }
    }
  }

}
